/** @format */

import React from 'react';

import {AppRegistry} from 'react-native';
import Navigator from './src/components/bottom-navigator';

const App=()=>(
        <Navigator/>
);
AppRegistry.registerComponent('reactnativedemo1', () => App);