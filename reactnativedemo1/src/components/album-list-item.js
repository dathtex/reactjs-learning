import React,{Component} from 'react';
import {Text, View,Image,Linking} from 'react-native';
import Card from './card';
import CardSection from './card-section';
import Button from './button';

class AlbumListItem extends Component{
    render() {

        const {thumbnail_image,title,artist,image,url}=this.props.album;

        return (
            <Card>
                <CardSection>
                    <Image source={{uri:image}}  resizeMode={'cover'}
                           style={{ width: '100%', height: 200 }}/>
                </CardSection>
                <CardSection>
                    <View style={styles.thumbnailView}>
                        <Image style={styles.thumbnailImage} source={{uri:thumbnail_image}}/>
                    </View>
                    <View style={styles.titleView}>
                        <Text style={styles.titleText}>{title}</Text>
                        <Text style={{fontSize: 10}}>{artist}</Text>
                    </View>
                    <View flex right style={styles.actionView}>
                        <Button onPress={()=>Linking.openURL(url)}>Buy Now</Button>
                    </View>
                </CardSection>

            </Card>
        );
    }
}

const styles={
    thumbnailView:{
        padding: 5
    },
    actionView:{
        paddingVertical: 8,
        paddingLeft: 10,
        width:100,
        maxWidth: 160,
        marginLeft: 'auto'

    },
    thumbnailImage:{
        borderRadius:20,
        width: 40,
        height: 40,
        paddingTop: 5
    },

    titleText:{
      fontSize:16,
      color:'#ff9500',
        fontWeight: 'bold'
    },
    titleView:{
        flexDirection: 'column',
        justifyContent: 'space-around',
        padding:5
    }
}


export default AlbumListItem;