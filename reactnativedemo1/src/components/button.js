import React from 'react';
import {Text,TouchableOpacity} from 'react-native';

const Button=({onPress,children})=>{
    return (
        <TouchableOpacity onPress={onPress} style={styles.buttonStyle} >
            <Text style={styles.textStyle}>{children}</Text>
        </TouchableOpacity>
    )
};

const styles={
    buttonStyle:{
        flex:1,
        alignSelf: 'stretch',
        backgroundColor:'#ffa20d',
        marginLeft: 4,
        marginRight: 4,
        borderRadius:6,
        padding: 6
    },
    textStyle:{
        alignSelf: 'center',
        color:'#fff',
        fontSize:16,
        padding: 3,
        fontWeight: '600'
    }



};
export default Button;