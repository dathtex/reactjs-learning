import React,{Component} from 'react';
import {Text, View,ScrollView} from 'react-native';
import AlbumListItem from './album-list-item';
import axios from 'axios';

class AlbumList extends Component{

    state={albums:[]};

    componentWillMount(): void {
        axios.get('https://rallycoding.herokuapp.com/api/music_albums').then(response=>{
            this.setState({albums:response.data});
        });
    }

    renderAlbums(){
        return this.state.albums.map(x=><AlbumListItem key={x.title} album={x} />);
    }

    render() {
        console.log(this.state);
        const {textStyle, viewStyle} = styles;
        return (
            <ScrollView style={viewStyle}>
                {this.renderAlbums()}
            </ScrollView>
        );
    }
}

const styles = {
    viewStyle: {
        backgroundColor:'#eee'
    },
    textStyle: {}

};

export default AlbumList;