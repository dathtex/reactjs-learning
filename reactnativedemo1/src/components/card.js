
import React,{Component} from 'react';
import {Text,View} from 'react-native';

class Card extends Component {
    render() {
        const {textStyle, containerStyle} = styles;
        return (
            <View style={containerStyle}>
                {this.props.children}
            </View>
        )
    }
}

const styles={
    containerStyle:{
        borderRadius:3,
        shadowColor: '#333',
        shadowOffset: {width:1,height:2},
        shadowOpacity: 0.3,
        shadowRadius: 2,
        elevation: 1,
        marginLeft: 10,
        marginRight: 10,
        marginTop: 10,
        marginBottom:6,
        overflow: 'hidden'

    },
    textStyle:{
        fontSize:20,
        color:'#fff'
    }

};

export default Card;