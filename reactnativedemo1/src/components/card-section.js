
import React,{Component} from 'react';
import {Text,View} from 'react-native';

class CardSection extends Component {
    render() {
        const {containerStyle} = styles;
        return (
            <View style={containerStyle}>
                {this.props.children}
            </View>
        )
    }
}

const styles={
    containerStyle: {
        padding: 0,
        backgroundColor:'#fff',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        position: 'relative'
    }
};

export default CardSection;