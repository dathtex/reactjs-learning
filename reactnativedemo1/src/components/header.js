
import React,{Component} from 'react';
import {Text,View} from 'react-native';

class Header extends Component {
    render() {
        const {textStyle, viewStyle} = styles;
        return (
            <View style={viewStyle}>
                <Text style={textStyle}>{this.props.headerText}</Text>
            </View>
        )
    }
}

const styles={
    viewStyle:{
        backgroundColor:'#d64526',
        alignItems: 'center',
        justifyContent:'center',
        height:80,
        paddingTop: 30,
        shadowColor: '#333',
        shadowOffset: {width:0,height: 2},
        shadowOpacity: 0.3,
        elevation: 2,
        position: 'relative'
    },
    textStyle:{
        fontSize:20,
        color:'#fff'
    }

};

export default Header;