import React from 'react';
import {Text, View} from 'react-native';
import {createBottomTabNavigator, createAppContainer} from 'react-navigation';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Header from './header';
import AlbumList from './album-list';


class HomeScreen extends React.Component {
    render() {
        return (
            <View style={{flex:1}}>
                <Header headerText={'MiFood.vn'}/>
                <AlbumList/>
            </View>
        );
    }
}

class OtherScreen extends React.Component {
    render() {
        return (
            <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                <Text>{this.props.text}</Text>
            </View>
        );
    }
}


const TabNavigator = createBottomTabNavigator({
        Home: HomeScreen,
        Video: OtherScreen,
        Account: OtherScreen,
        Message: OtherScreen,
        Settings: OtherScreen,
    },
    {
        defaultNavigationOptions: ({navigation}) => ({
            tabBarIcon: ({focused, horizontal, tintColor}) => {
                const {routeName} = navigation.state;
                let IconComponent = FontAwesome5;
                let iconName=genIcons(routeName);
                // You can return any component that you like here!
                return <IconComponent name={iconName} size={25} color={tintColor}/>;
            },
        }),
        tabBarOptions: {
            activeTintColor: 'tomato',
            inactiveTintColor: 'gray',
            style:{
                paddingTop: 10
            }
        },
    }
);

function genIcons(name) {
    switch (name) {
        case 'Home':return 'home';
        case 'Settings': return 'cog';
        case  'Video': return 'video';
        case 'Message': return 'facebook-messenger';
        case 'Account': return 'user';
    }
}

export default createAppContainer(TabNavigator);